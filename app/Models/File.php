<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $file_size
 * @property string $created_at
 * @property string $updated_at
 */
class File extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = ['title', 'file_size'];

    public function getFileSize(int $decimals = 2): string
    {
        $sz = 'BKMGTP';
        $factor = floor((strlen($this->file_size) - 1) / 3);
        return sprintf("%.{$decimals}f", $this->file_size / pow(1024, $factor)) . @$sz[$factor];
    }
}
