<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\View\View;

class FilesController extends Controller
{
    public function uploadForm(): View
    {
        $files = File::orderBy('created_at', 'desc')->take(10)->get();
        return view('forms.upload-form', compact('files'));
    }

    public function upload(Request $request): RedirectResponse
    {
        $files = $request->allFiles();

        if (empty($files)) {
            return back()
                ->withErrors(['msg' => 'Добавьте хотя бы один файл!']);
        }

        foreach ($files['file'] as $file) {
            $fileName = time() . '_' . $file->getClientOriginalName();
            /** @var UploadedFile $file */
            $file->move(storage_path('app/public'), $fileName);

            $item = new File([
                'title' => $fileName,
                'file_size' => $file->getSize()
            ]);

            if (!$item->save()) {
                return back()
                    ->withErrors(['msg' => 'Ошибка сохранения']);
            }
        }

        return redirect()
            ->route('upload_form')
            ->with(['success' => 'Успешно сохранено']);
    }
}
