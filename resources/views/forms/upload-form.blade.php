@extends('layouts.app')

@section('content')
    <div class="container">
        @if($errors->any())
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span>x</span>
                        </button>
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
        @endif

        @if(session('success'))
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span>x</span>
                        </button>
                        {{session()->get('success')}}
                    </div>
                </div>
            </div>
        @endif


        <div class="row justify-content-center">
            <div class="col-md-12">
                <div>
                    <form method="post" action="{{ route('upload_file') }}" enctype="multipart/form-data">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                        <input type="file" multiple name="file[]">
                        <button class="btn btn-primary" type="submit">Загрузить</button>
                    </form>
                </div>
                <br>
                <div>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td>Имя файла</td>
                            <td>Размер</td>
                            <td>Дата загрузки</td>
                        </tr>
                        </thead>
                        <tbody>
                        @php /** @var \App\Models\File[] $files */ @endphp
                        @foreach($files as $item)
                            <tr>
                                <td><a href="storage/{{$item->title}}" target="_blank">{{$item->title}}</a></td>
                                <td>{{$item->getFileSize()}}</td>
                                <td>{{$item->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
@endsection
