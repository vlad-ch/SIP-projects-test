FROM php:7.4-fpm

RUN apt update
RUN apt install -y apt-utils

# Install dependencies
RUN apt install -qq -y \
  curl \
  git \
  zlib1g-dev \
  zip unzip libzip-dev \
  libonig-dev \
  libxml2-dev

# Clear cache
RUN apt clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install \
  bcmath \
  pdo_mysql \
  pcntl \
  zip \
  pdo \
  ctype \
  mbstring \
  tokenizer \
  fileinfo \
  xml \
  intl

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /var/www

WORKDIR /var/www

RUN mkdir /var/www/node_modules

RUN chown -R www-data:www-data \
  /var/www/storage \
  /var/www/bootstrap/cache \
  /var/www/node_modules

CMD php-fpm
